package es.paradigma.samples.model;

import java.util.ArrayList;
import java.util.List;

public class Podium {

	private Runner firstPositionRunner;
	private Runner secondPositionRunner;
	private Runner thirdPositionRunner;
	
	public Podium() {
		super();
	}

	public Runner getFirstPositionRunner() {
		return firstPositionRunner;
	}

	public void setFirstPositionRunner(Runner firstPositionRunner) {
		this.firstPositionRunner = firstPositionRunner;
	}

	public Runner getSecondPositionRunner() {
		return secondPositionRunner;
	}

	public void setSecondPositionRunner(Runner secondPositionRunner) {
		this.secondPositionRunner = secondPositionRunner;
	}

	public Runner getThirdPositionRunner() {
		return thirdPositionRunner;
	}

	public void setThirdPositionRunner(Runner thirdPositionRunner) {
		this.thirdPositionRunner = thirdPositionRunner;
	}
	
	public boolean isEmpty() {
		return firstPositionRunner == null && 
				secondPositionRunner == null && 
				thirdPositionRunner == null;
	}
	
	public void addRunner(Runner runner) {
		if(firstPositionRunner == null) {
			firstPositionRunner = runner;
		} else if(secondPositionRunner == null) {
			secondPositionRunner = runner;
		} else if(thirdPositionRunner == null) {
			thirdPositionRunner = runner;
		}
	}
	
	public List<Runner> getRunners(){
		List<Runner> runners = new ArrayList<Runner>();
		if (firstPositionRunner != null) {
			runners.add(firstPositionRunner);
		}
		if (secondPositionRunner != null) {
			runners.add(secondPositionRunner);
		}
		if (thirdPositionRunner != null) {
			runners.add(thirdPositionRunner);
		}
		return runners;
	}

	@Override
	public String toString() {
		return "1� " + firstPositionRunner + 
				"\n2� " + secondPositionRunner +
				"\n3� " + thirdPositionRunner;
	}
	
	
}
