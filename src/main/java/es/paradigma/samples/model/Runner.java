package es.paradigma.samples.model;

public class Runner {

	private Integer dorsal;
	private String name;
	private String surname;
	private Long time;
	private Long penalty;
	private Long endTime;
	
	public Runner(int dorsal, String name, String surname, long time, long penalty) {
		super();
		this.dorsal = dorsal;
		this.name = name;
		this.surname = surname;
		this.time = time;
		this.penalty = penalty;
	}

	public int getDorsal() {
		return dorsal;
	}

	public void setDorsal(int dorsal) {
		this.dorsal = dorsal;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long getPenalty() {
		return penalty;
	}

	public void setPenalty(long penalty) {
		this.penalty = penalty;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
	
	@Override
	public String toString() {
		return "Runner [dorsal=" + dorsal + ", name=" + name + ", surname=" + surname + ", time=" + time
				+ ", penalty=" + penalty + ", endTime=" + endTime + "]";
	}
	
}
