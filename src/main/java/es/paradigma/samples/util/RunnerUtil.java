package es.paradigma.samples.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import es.paradigma.samples.model.Runner;

public final class RunnerUtil {

	private RunnerUtil() {}
	
	public static Runner addPenalty(Runner runner) {
		runner.setEndTime(runner.getTime() + runner.getPenalty());
		return runner;
	}
	
	public static Comparator<Runner> getRunnerComparator() {
		return new Comparator<Runner>() {
			@Override
			public int compare(Runner r1, Runner r2) {
				int endTime = r1.getEndTime() > r2.getEndTime() ? 1 : 
					r1.getEndTime() < r2.getEndTime() ? -1 : 0;
				int penalty = r1.getPenalty() > r2.getPenalty() ? 1 : 
					r1.getPenalty() < r2.getPenalty() ? -1 : 0;
				int dorsal = r1.getDorsal() > r2.getDorsal() ? 1 : -1;
				return endTime != 0 ? endTime : penalty != 0 ? penalty : dorsal;
			}
		};
	}
	
	public static List<Runner> getRunners(){
		Runner runnerOne = new Runner(1, "Mario", "Sanchez", 300L, 2L);
		Runner runnerTwo = new Runner(2, "Daniel", "Jimenez", 304L, 1L);
		Runner runnerThree = new Runner(3, "Jorge", "Gonzalez", 307L, 1L);
		Runner runnerFour = new Runner(4, "David", "Ruiz", 310L, 2L);
		Runner runnerFive = new Runner(5, "Juan", "Fernandez", 308L, 0L);
		Runner runnerSix = new Runner(6, "Adrian", "Lopez", 308L, 0L);
		
		List<Runner> runners = new ArrayList<Runner>();
		runners.add(runnerOne);
		runners.add(runnerTwo);
		runners.add(runnerThree);
		runners.add(runnerFour);
		runners.add(runnerFive);
		runners.add(runnerSix);
		
		return runners;
	}
}
