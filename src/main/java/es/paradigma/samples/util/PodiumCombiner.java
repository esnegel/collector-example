package es.paradigma.samples.util;

import java.util.List;

import es.paradigma.samples.model.Podium;
import es.paradigma.samples.model.Runner;

public final class PodiumCombiner {

	private PodiumCombiner() {}
	
	public static Podium combinePodiums(Podium first, Podium second) {
		Podium podium = new Podium();
		List<Runner> runners = first.getRunners();
		runners.addAll(second.getRunners());
		runners.stream().sorted(RunnerUtil.getRunnerComparator())
			.limit(3L).forEach(r -> podium.addRunner(r));
		return podium;
	}
}
