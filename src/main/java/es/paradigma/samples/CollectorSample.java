package es.paradigma.samples;

import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collector.Characteristics;

import es.paradigma.samples.collector.RunnerCollector;
import es.paradigma.samples.model.Podium;
import es.paradigma.samples.model.Runner;
import es.paradigma.samples.util.RunnerUtil;

public class CollectorSample {

	public static void main(String[] args) {
		List<Runner> runners = RunnerUtil.getRunners();
		
		RunnerCollector runnerCollector = new RunnerCollector();
		
		Set<Characteristics> characteristics = runnerCollector.characteristics();
		
		Podium podium = runners.parallelStream().collect(Collector.of(
				runnerCollector.supplier(), 
				runnerCollector.accumulator(), 
				runnerCollector.combiner(),
				runnerCollector.finisher(),
				characteristics.toArray(new Characteristics[characteristics.size()])));

		System.out.println(podium);
	}
}
