package es.paradigma.samples.collector;

import es.paradigma.samples.model.Podium;
import es.paradigma.samples.model.Runner;
import es.paradigma.samples.util.PodiumCombiner;
import es.paradigma.samples.util.RunnerUtil;

public class RunnerAccumulator {
	
	private Podium podium;

	public RunnerAccumulator() {
		podium = new Podium();
	}
	
	public void accumulate(Runner runner) {
		podium.addRunner(RunnerUtil.addPenalty(runner));
	}
	
	public RunnerAccumulator combine(RunnerAccumulator other) {
		podium = PodiumCombiner.combinePodiums(podium, other.finish());
		return this;
	}
	
	public Podium finish() {
		return this.podium;
	}
}
